using System.Collections;
using UnityEngine;
using TMPro;

public class UI_Script : MonoBehaviour
{
    [Header("Main UI Objects")]
    public GameObject openChest_UI;
    public GameObject openChest_UI_noKey;
    public GameObject takeKey_UI;
    public GameObject needKey_UI;
    public GameObject openDoor_UI;
    public GameObject timer_UI;

    [Header("Loading UI Objects")]
    [SerializeField] private GameObject loading_UI;

    [Header("Menu UI Objects")]
    [SerializeField] private GameObject startGame_UI;
    [SerializeField] TextMeshProUGUI bestTime_UI;

    [Header("Game Over UI Objects")]
    [SerializeField] private GameObject gameOver_UI;

    [SerializeField]
    private StateMachine stateMachine;

    public static UI_Script UI_Instance { get; private set; }

    private void Awake()
    {
        if (UI_Instance == null)
            UI_Instance = this;

    }
    public void OpenWindow(GameObject obj)
    {
        obj.SetActive(true);
        stateMachine.ChangeState(new UIOnScreen());
        FindObjectOfType<AudioManager>().Play("Open_Window");
    }

    public void closeWindow(GameObject obj)
    {
        obj.SetActive(false);
        stateMachine.ChangeState(new MainState());
        FindObjectOfType<AudioManager>().Play("Open_Window");
    }

    public void SetupLoadingUI()
    {
        loading_UI.gameObject.SetActive(true);
        startGame_UI.gameObject.SetActive(false);
    }

    public void SetupMainUI()
    {
        loading_UI.gameObject.SetActive(false);
    }

    public void SetupStartGameUI()
    {
        bestTime_UI.text = "Highscore: " + string.Format("{0:0.00}", PlayerPrefs.GetFloat("Highscore"));
        startGame_UI.gameObject.SetActive(true);
        loading_UI.gameObject.SetActive(false);
    }
    public void SetupGameOverState()
    {
        stateMachine.ChangeState(new GameOverState());
    }
    public void SetupGameOverUI()
    {
        StartCoroutine(Delay_GameOver());
    }

    public void StartGameButton()
    {
        stateMachine.ChangeState(new LoadingState());
    }

    IEnumerator Delay_GameOver()
    {

        yield return new WaitForSeconds(0.4f);
        gameOver_UI.gameObject.SetActive(true);
    }
}
