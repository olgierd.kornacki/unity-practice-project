using UnityEngine;

public class Player_Movement_Controller : MonoBehaviour
{
    [Header("Camera")]
    [SerializeField] private Camera playerCamera;
    [SerializeField] private float cameraSpeed;

    [Header("Movement and Collisions")]
    [SerializeField] private Rigidbody playerRb;
    [SerializeField] private float playerSpeed;

    [HideInInspector] public Vector3 movement;

    private void Update()
    {
        Vector2 movementInput = GameController.Instance.Controls.Player.WSAD.ReadValue<Vector2>();
        float playerRotation = GameController.Instance.Controls.Player.Rotation.ReadValue<float>();
        if (GameController.Instance.canMove)
        {
            Player_Move(movementInput);
            Player_Rotation(playerRotation);
        }
    }

    private void Player_Move(Vector2 input)
    {
        movement = new Vector3(input.x, 0.0f, input.y);
        transform.Translate(movement * playerSpeed * Time.deltaTime, Space.Self);
    }

    private void Player_Rotation(float input)
    {
        transform.Rotate(Vector3.up, input * cameraSpeed * Time.deltaTime);
    }
}
