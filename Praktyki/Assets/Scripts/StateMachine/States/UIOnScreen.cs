using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIOnScreen : BaseState
{

    public override void PrepareState()
    {
        base.PrepareState();
        GameController.Instance.canMove = false;
    }

    public override void UpdateState()
    {
        base.UpdateState();
    }

    public override void DestroyState()
    {
        base.DestroyState();
        GameController.Instance.canMove = true;
    }

}
