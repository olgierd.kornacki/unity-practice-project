using UnityEngine;

public class LoadingState : BaseState
{

    private float loadingTime = 1.25f;

    public override void PrepareState()
    {
        base.PrepareState();
        UI_Script.UI_Instance.SetupLoadingUI();
        Time.timeScale = 0f;
    }

    public override void UpdateState()
    {
        base.UpdateState();
        loadingTime -= Time.unscaledDeltaTime;
        if (loadingTime <= 0.0f)
        {
            owner.ChangeState(new MainState());
        }

    }


}
