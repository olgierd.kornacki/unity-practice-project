using UnityEngine;

public class StartGameState : BaseState
{

    public override void PrepareState()
    {
        UI_Script.UI_Instance.SetupStartGameUI();
        base.PrepareState();


    }

    public override void UpdateState()
    {
        base.UpdateState();

    }
}
