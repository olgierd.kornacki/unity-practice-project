using UnityEngine;

public class GameOverState : BaseState
{
    public override void PrepareState()
    {
        GameController.Instance.startTimer = false;
        GameController.Instance.SetPlayerPrefs();
        base.PrepareState();
        UI_Script.UI_Instance.SetupGameOverUI();
    }

    public override void UpdateState()
    {
        base.UpdateState();
    }
}
