using UnityEngine;

public class MainState : BaseState
{
    public override void PrepareState()
    {
        GameController.Instance.startTimer = true;
        base.PrepareState();
        UI_Script.UI_Instance.SetupMainUI();
        Time.timeScale = 1.0f;
    }

    public override void UpdateState()
    {
        base.UpdateState();
    }
}
