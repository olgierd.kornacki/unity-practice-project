using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [Header("Objects")]
    [SerializeField] private GameObject doorGameObject;
    [SerializeField] private GameObject chestGameObject;

    [SerializeField] private float radius = 1;

    private void Start()
    {
        SpawnDoor();
        SpawnChest();
    }
    void SpawnDoor()
    {
        Vector3 spawnPoint = new Vector3();
        int n = Random.Range(0, 2);
        if (n == 0)
        {
            float horizontal = Random.Range(0, 2);
            float vertical = Random.Range(0.1f, 0.9f);
            spawnPoint = new Vector3(horizontal * radius, 0, vertical * radius);
            doorGameObject.transform.position = transform.position + spawnPoint;
            doorGameObject.transform.eulerAngles = new Vector3(0, 90, 0);
            if (horizontal == 1f)
            {
                doorGameObject.transform.eulerAngles = new Vector3(0, -90, 0);
            }
            else
            {
                doorGameObject.transform.eulerAngles = new Vector3(0, 90f, 0);
            }
        }
        else
        {
            float horizontal = Random.Range(0.1f, 0.9f);
            float vertical = Random.Range(0, 2);
            spawnPoint = new Vector3(horizontal * radius, 0, vertical * radius);
            doorGameObject.transform.position = transform.position + spawnPoint;
            if (vertical == 1f)
            {
                doorGameObject.transform.eulerAngles = new Vector3(0, 180, 0);
            }
            else
            {
                doorGameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            }
        }

    }
    void SpawnChest()
    {
        float horizontal = Random.Range(0.1f * radius, 0.9f * radius);
        float vertical = Random.Range(0.1f * radius, 0.9f * radius);
        float offset = Random.Range(0,360);
        Vector3 spawnPoint = new Vector3(horizontal, 0, vertical);
        chestGameObject.transform.position = transform.position + spawnPoint;
        chestGameObject.transform.eulerAngles = new Vector3(0, offset, 0);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(this.transform.position + new Vector3(radius/2, 0, radius/2), new Vector3(radius, 1, radius));
    }
}
