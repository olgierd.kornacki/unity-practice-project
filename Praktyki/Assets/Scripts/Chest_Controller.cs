using UnityEngine;

public class Chest_Controller : MonoBehaviour
{
    private Animator chestAnim;
    private void Start()
    {
        chestAnim = GetComponent<Animator>();
    }

    public void OpenChest()
    {
        chestAnim.SetBool("chestOpen", true);
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
    }

    public void CloseChest()
    {
        chestAnim.SetBool("chestOpen", false);
        this.gameObject.GetComponent<BoxCollider>().enabled = true;
    }

}
