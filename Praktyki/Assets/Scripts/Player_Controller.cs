using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{
    private Camera playerCamera;

    [SerializeField] private float mouseInteractionRange;

    public bool playerHasKey { get; set; }

    private Color objColor;

    UI_Script setUI;
    private void Start()
    {
        setUI = UI_Script.UI_Instance;

        GameController.Instance.Controls.Player.MouseClick.performed += _ => MouseClick(MouseObj(getMousePosition()));

        playerCamera = GetComponentInChildren<Camera>();
    }
    private void Update()
    {
        MouseHoverOverObject(MouseObj(getMousePosition()));
    }

    private void MouseClick(RaycastHit mObject)
    {

        if (mObject.collider != null && mObject.collider.CompareTag("Chest"))
        {
            if (playerHasKey)
                setUI.OpenWindow(setUI.openChest_UI_noKey);
            else
                setUI.OpenWindow(setUI.openChest_UI);
        }
        if (mObject.collider != null && mObject.collider.CompareTag("Key"))
        {
            setUI.OpenWindow(setUI.takeKey_UI);
        }
        if (mObject.collider != null && mObject.collider.CompareTag("Door"))
        {
            if (playerHasKey)
                setUI.OpenWindow(setUI.openDoor_UI);
            else
                setUI.OpenWindow(setUI.needKey_UI);
        }
        else
            return;
    }
    private void MouseHoverOverObject(RaycastHit hoverObject)
    {

        if (hoverObject.collider != null)
        {
            // Debug.Log(objColor);
            //objColor = hoverObject.collider.GetComponentInChildren<Renderer>().material.color;
            // hoverObject.collider.GetComponentInChildren<Renderer>().material.color = new Color(objColor.r + 30f, objColor.g + 30, objColor.b + 30, objColor.a);
        }
    }

    Vector2 getMousePosition()
    {
        return GameController.Instance.Controls.Player.MousePos.ReadValue<Vector2>();
    }

    RaycastHit MouseObj(Vector2 mousePos)
    {
        Ray ray = playerCamera.ScreenPointToRay(mousePos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, mouseInteractionRange))
            return hit;
        else
            return hit = new RaycastHit();

    }
}
