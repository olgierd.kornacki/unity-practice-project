using UnityEngine;
using TMPro;
public class Timer_Controller : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI timer;
    [SerializeField] TextMeshProUGUI highestScore;
    [SerializeField] TextMeshProUGUI actualScore;

    private void Start()
    {
        GameController.Instance.playerScore = 0;
        highestScore.text = "Highscore: " + string.Format("{0:0.00}", PlayerPrefs.GetFloat("Highscore"));
        Debug.Log(PlayerPrefs.GetFloat("Highscore"));
    }
    private void Update()
    {
        if (GameController.Instance.startTimer)
        {
            GameController.Instance.playerScore += Time.deltaTime;
            timer.text = string.Format("{0:0.00}", GameController.Instance.playerScore);
        }
        actualScore.text = "Score: " + string.Format("{0:0.00}", GameController.Instance.playerScore);
    }
}
