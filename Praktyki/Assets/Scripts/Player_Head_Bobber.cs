using UnityEngine;

public class Player_Head_Bobber : MonoBehaviour
{
    [SerializeField] private float frequency, intensity;

    Player_Movement_Controller pmc;
    float defaultPosY, timer;

    void Start()
    {
        pmc = GetComponentInParent<Player_Movement_Controller>();
        defaultPosY = transform.localPosition.y;
    }

    void Update()
    {
        if (GameController.Instance.canMove)
            CameraShake();
    }

    private void CameraShake()
    {
        if (Mathf.Abs(pmc.movement.x) > 0.1f || Mathf.Abs(pmc.movement.z) > 0.1f)
        {
            timer += Time.deltaTime * frequency;
            transform.localPosition = new Vector3(transform.localPosition.x, defaultPosY + Mathf.Sin(timer) * intensity, transform.localPosition.z);
        }
        else
        {
            timer = 0;
            transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Lerp(transform.localPosition.y, defaultPosY, Time.deltaTime * frequency), transform.localPosition.z);
        }
    }
}

