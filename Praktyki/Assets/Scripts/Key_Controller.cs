using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key_Controller : MonoBehaviour
{
    [SerializeField] private GameObject destroyEffect;

    public void TakeKey(GameObject playerGameObject)
    {
        Instantiate(destroyEffect, transform.position, Quaternion.Euler(-90, 0, 0));
        playerGameObject.GetComponent<Player_Controller>().playerHasKey = true;
        Destroy(this.gameObject, 0.1f);
    }
}
