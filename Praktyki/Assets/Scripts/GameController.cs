using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    public InputMaster Controls { get; private set; }

    public bool startTimer = false;

    public bool canMove { get; set; } = true;

    public float playerScore;

    private void Awake()
    {
        if (Controls == null)
            Controls = new InputMaster();
        if (Instance == null)
            Instance = this;
    }
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void SetPlayerPrefs()
    {
        if (PlayerPrefs.GetFloat("Highscore") > playerScore)
        {
            PlayerPrefs.SetFloat("Highscore", playerScore);
            Debug.Log("New Highscore");
        }
        else if (PlayerPrefs.GetFloat("Highscore") == 0f)
        {
            PlayerPrefs.SetFloat("Highscore", playerScore);
            Debug.Log("Set first Highscore");
        }
    }
    private void OnEnable()
    {
        Controls.Enable();
    }

    private void OnDisable()
    {
        Controls.Disable();
    }
}
